package com.example.retrofitdemo1

import com.google.gson.annotations.SerializedName

class CategoryResponse {

    @SerializedName("status")
    var status: String? = null

    @SerializedName("count")
    var count: Int = 0

    @SerializedName("categories")
    var categories: List<Category>? = null

}