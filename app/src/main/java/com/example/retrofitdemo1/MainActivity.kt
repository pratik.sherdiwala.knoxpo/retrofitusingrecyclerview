package com.example.retrofitdemo1

import android.app.ProgressDialog
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback

class MainActivity : AppCompatActivity() {

    companion object {

        val TAG = MainActivity::class.java.simpleName
    }

    var categoryItems = mutableListOf<Category>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        itemsRV.layoutManager = LinearLayoutManager(this)
        itemsRV.adapter = CategoryAdapter(categoryItems)

        clickBTN.setOnClickListener(View.OnClickListener {
            DisplayProgressDialog()
            callWebService()
        })
    }

    fun callWebService() {
        val apiService = ApiInterface.create()
        val call = apiService.getCategoryDetails()
        Log.d("REQUEST", call.toString() + "")
        call.enqueue(object : Callback<CategoryResponse> {
            override fun onResponse(call: Call<CategoryResponse>, response: retrofit2.Response<CategoryResponse>?) {
                if (response != null) {
                    if (pDialog != null && pDialog!!.isShowing()) {
                        pDialog.dismiss()
                    }
                    categoryItems.clear()
                    categoryItems.addAll(response.body()!!.categories!!)

                    itemsRV.adapter?.notifyDataSetChanged()
//                    Log.d("MainActivity", "" + categoryItems.size)
//                    var msg: String = ""
//
//                    for (item: Category in categoryItems) {
//                        msg = msg + item.title + " "+item.parent+"\n"
//                    }
//                    Toast.makeText(this@MainActivity, "List of Category  \n  " + msg, Toast.LENGTH_LONG).show()
                }
            }

            override fun onFailure(call: Call<CategoryResponse>, t: Throwable) {
                //                Log.e(TAG, t.toString());
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss()
                }
                Toast.makeText(this@MainActivity, "Error", Toast.LENGTH_SHORT).show()
                Log.e(TAG, "onFailure()", t)
            }
        })
    }

    lateinit var pDialog: ProgressDialog

    fun DisplayProgressDialog() {
        pDialog = ProgressDialog(this@MainActivity)
        pDialog!!.setMessage("Loading...")
        pDialog!!.setCancelable(false)
        pDialog!!.isIndeterminate = false
        pDialog!!.show()
    }
}