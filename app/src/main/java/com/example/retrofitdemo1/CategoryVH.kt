package com.example.retrofitdemo1

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class CategoryVH(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val categoryTV=itemView.findViewById<TextView>(R.id.categoryTV)

    fun bindCatewgory(category:Category){
        categoryTV.text=category.title
    }
}