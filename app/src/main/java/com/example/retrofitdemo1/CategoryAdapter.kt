package com.example.retrofitdemo1

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class CategoryAdapter(private val items: List<Category>) : RecyclerView.Adapter<CategoryVH>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryVH {

        return CategoryVH(
            LayoutInflater
                .from(parent.context)
                .inflate(
                    R.layout.list_category,
                    parent,
                    false
                )
        )
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: CategoryVH, position: Int) {

        holder.bindCatewgory(items.get(position))
    }

}